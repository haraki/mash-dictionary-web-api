#
# Output.pm
#
#        author : Masashi Haraki
#

package Output;

use strict;
use warnings;
use utf8;

# コンストラクタ
#  return    : インスタンス
sub new
{
	my ($myclass, $cgi_query) = @_;
	
	if(!defined($cgi_query))
	{
		$cgi_query = new CGI();
	}

	# メンバ
	my $member =
	{
		'cgi_query' => $cgi_query,
		'buffer'    => '',
	};
	
	my $this = bless $member, $myclass;

	return $this;
}

sub header
{
	return -1;
}

sub set_empty
{
	return -1;
}

sub flush
{
	my ($this) = @_;
	
	print $this->{'buffer'} . "\n";
	
	$this->{'buffer'} = '';
	
	return 0;
}

1;

