#
# Api.pm
#
#        author : Masashi Haraki
#

package Api;

use strict;
use warnings;
use utf8;

use CGI;

use Param;
use File;
use Output;

my %method_table = (
	'GET' => \&_get,
);

my %output_class_table = (
	'json' => "Output::Json",
	'xml'  => "Output::Xml",
);

my $q;
my $param;
my $file;

sub api
{
	$q = new CGI();
	$param = new Param($q);
	
	my $method = $param->get_param('REQUEST_METHOD');
	if(!defined($method))
	{
		$method = 'GET';
	}
	
	if(exists($method_table{$method}))
	{
		$method_table{$method}();
	}
	else
	{
		print $q->header(-status => '405 Method Not Allowed');
	}
	
	return 0;
}

sub _get
{
	my $str  = '';
	my %param;
	if(_get_param(\%param) < 0)
	{
		$param{'type'} = 'json';
		
		goto END;
	}
	
	my $output = _create_module($output_class_table{$param{'type'}});
	
	if(!defined($param{'name'}))
	{
		$output->set_empty();
		
		goto END;
	}
	
END:
	$output->header();
	$output->flush();
	
	return 0;
}

sub _get_param
{
	my ($ref_param) = @_;
	
	my $ref_path_info = $param->get_path_info();
	
	$$ref_param{'name'} = $$ref_path_info[0];
	
	my $type = $$ref_path_info[1];
	if(!defined($type))
	{
		$$ref_param{'type'} = 'json';
	}
	else
	{
		$$ref_param{'type'} = lc($type);
		if(!exists($output_class_table{$$ref_param{'type'}}))
		{
			$$ref_param{'type'} = 'json';
		}
	}
	
	return 0;
}

sub _create_module
{
	my ($module) = @_;
	
	my $path = $module . ".pm";
	
	$path =~ s/::/\//g;
	
	require $path;
	
	return "$module"->new($q);
}

1;
