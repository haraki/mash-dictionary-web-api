#
# Output/Xml.pm
#
#        author : Masashi Haraki
#

package Output::Xml;
our @ISA = "Output";

use strict;
use warnings;
use utf8;

# コンストラクタ
#  return    : インスタンス
sub new
{
	my ($myclass, $cgi_query) = @_;

	my $this = $myclass->SUPER::new($cgi_query);

	return $this;
}

sub header
{
	my ($this) = @_;
	
	print $this->{'cgi_query'}->header(-type => 'text/xml', -charset => 'utf-8');
	print "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
	
	return 0;
}

sub set_empty
{
	my ($this) = @_;
	
	$this->{'buffer'} = "<header></header><data></data>";
	
	return 0;
}

1;

