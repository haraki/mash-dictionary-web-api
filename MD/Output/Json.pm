#
# Output/Json.pm
#
#        author : Masashi Haraki
#

package Output::Json;
our @ISA = "Output";

use strict;
use warnings;
use utf8;

# コンストラクタ
#  return    : インスタンス
sub new
{
	my ($myclass, $cgi_query) = @_;

	my $this = $myclass->SUPER::new($cgi_query);

	return $this;
}

sub header
{
	my ($this) = @_;
	
	print $this->{'cgi_query'}->header(-type => 'application/json', -charset => 'utf-8');
	
	return 0;
}

sub set_empty
{
	my ($this) = @_;
	
	$this->{'buffer'} = "{}";
	
	return 0;
}

1;

