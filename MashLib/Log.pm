#
# MashLib::Log.pm
#
#        author : Masashi Haraki
#

package Log;

use strict;
use warnings;
use utf8;

use File;

use constant DEFAULT_FORMAT => '{DATE}T{TIME} [{LEVEL}] {FILE}:{LINE} {MESSAGE}';

# コンストラクタ
#  param[in] : ログファイル名(undef の場合はSTDOUTにのみ出力)
#  param[in] : 出力フォーマット(undef の場合はデフォルト)
#  return    : インスタンス
sub new
{
	my ($myclass, $filename, $format) = @_;
	
	my $file_obj = undef;
	if((defined($filename)) && ($filename ne ''))
	{
		$file_obj = new File($filename);
	}
	
	if((!defined($format)) || ($format eq ''))
	{
		$format = DEFAULT_FORMAT;
	}
	
	# メンバ
	my $member =
	{
		'file_obj' => $file_obj,
		'format'   => $format,
		'file_out' => { 'trace' => 1, 'debug' => 1, 'info' => 1, 'warn' => 1, 'error' => 1, 'fatal' => 1, },
	};
	
	return bless $member, $myclass;
}

# 出力フォーマット設定
#  param[in] : 出力フォーマット
#  return    : 結果
sub set_format
{
	my ($this, $format) = @_;
	
	if((!defined($format)) || ($format eq ''))
	{
		$format = DEFAULT_FORMAT;
	}
	
	$this->{'format'} = $format;
	
	return 0;
}

# 指定レベルのファイル出力を有効化
#  param[in] : 有効化するレベル('trace', 'debug', 'info', 'warn', 'error' or 'fatal') 複数指定可
#  return    : 結果
sub enable_file_out
{
	my ($this, @levels) = @_;
	my $ret = 0;
	
	foreach my $level (@levels)
	{
		if(!exists($this->{'file_out'}{$level}))
		{
			return -1;
		}
		
		$this->{'file_out'}{$level} = 1;
	}
	
	return 0;
}

# 指定レベルのファイル出力を無効化
#  param[in] : 無効化するレベル('trace', 'debug', 'info', 'warn', 'error' or 'fatal') 複数指定可
#  return    : 結果
sub disable_file_out
{
	my ($this, @levels) = @_;
	my $ret = 0;
	
	foreach my $level (@levels)
	{
		if(!exists($this->{'file_out'}{$level}))
		{
			return -1;
		}
		
		$this->{'file_out'}{$level} = 0;
	}
	
	return 0;
}

# trace 出力
#  param[in] : 文字列
sub trace
{
	my ($this, $message) = @_;
	my ($pkg, $file, $line) = caller();
	
	$this->_print("trace", $pkg, $file, $line, $message);
}

# debug 出力
#  param[in] : 文字列
sub debug
{
	my ($this, $message) = @_;
	my ($pkg, $file, $line) = caller();
	
	$this->_print("debug", $pkg, $file, $line, $message);
}

# info 出力
#  param[in] : 文字列
sub info
{
	my ($this, $message) = @_;
	my ($pkg, $file, $line) = caller();
	
	$this->_print("info", $pkg, $file, $line, $message);
}

# warn 出力
#  param[in] : 文字列
sub warn
{
	my ($this, $message) = @_;
	my ($pkg, $file, $line) = caller();
	
	$this->_print("warn", $pkg, $file, $line, $message);
}

# error 出力
#  param[in] : 文字列
sub error
{
	my ($this, $message) = @_;
	my ($pkg, $file, $line) = caller();
	
	$this->_print("error", $pkg, $file, $line, $message);
}

# fatal 出力
#  param[in] : 文字列
sub fatal
{
	my ($this, $message) = @_;
	my ($pkg, $file, $line) = caller();
	
	$this->_print("fatal", $pkg, $file, $line, $message);
}

# 出力
#  param[in] : 文字列
sub _print
{
	my ($this, $level, $pkg, $file, $line, $message) = @_;
	my @time = localtime();
	my $date_str = sprintf("%04d-%02d-%02d", $time[5] + 1900, $time[4] + 1, $time[3]);
	my $time_str = sprintf("%02d:%02d:%02d", $time[2],        $time[1],     $time[0]);
	my $string   = $this->{'format'};
	
	$string =~ s/\{DATE\}/$date_str/;
	$string =~ s/\{TIME\}/$time_str/;
	$string =~ s/\{LEVEL\}/$level/;
	$string =~ s/\{PACKAGE\}/$pkg/;
	$string =~ s/\{FILE\}/$file/;
	$string =~ s/\{LINE\}/$line/;
	$string =~ s/\{MESSAGE\}/$message/;
	$string .= "\n";
	
	print $string;
	
	if((defined($this->{'file_obj'})) && ($this->{'file_out'}{$level}))
	{
		$this->{'file_obj'}->push_back($string);
	}
}

1;
