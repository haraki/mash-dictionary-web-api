#
# MashLib::Util.pm
#
#        author : Masashi Haraki
#

package Util;

use strict;
use warnings;
use utf8;

use URI::Escape;

use Exporter;
our @ISA = qw/Exporter/;
our @EXPORT = qw/encode_url decode_url escape_html unescape_html get_env_names get_env get_envs/;

# URLエンコード
#  param[in] : エンコードしたい文字列
#  return    : エンコードした文字列
sub encode_url
{
	my ($data) = @_;

	return uri_escape_utf8($data);
}

# URLデコード
#  param[in] : デコードしたい文字列
#  return    : デコードした文字列
sub decode_url
{
	my ($data) = @_;

	$data = uri_unescape($data);
	utf8::decode($data);

	return $data;
}

# HTMLエスケープ
#  param[in] : 文字列
#  return    : エスケープした文字列
sub escape_html
{
	my ($str) = @_;
	
	CGI::charset("utf-8");
	$str = CGI::escapeHTML($str);
	
	$str =~ s/\'/&#39;/g;			# シングルクォーテーションは CGI::escapeHTML() では正常にエスケープされない
	
	return $str;
}

# HTMLアンエスケープ
#  param[in] : 文字列
#  return    : アンエスケープした文字列
sub unescape_html
{
	my ($str) = @_;
	
	$str =~ s/&#39;/\'/g;			# シングルクォーテーションは CGI::unescapeHTML() では正常にアンエスケープされない
	
	CGI::charset("utf-8");
	$str = CGI::unescapeHTML($str);
	
	return $str;
}

# 環境変数の名前一覧の取得
#  return    : 名前一覧の配列のリファレンス
sub get_env_names
{
	my @names = keys(%ENV);
	
	return \@names;
}

# 環境変数を取得
#  param[in] : 環境変数名
#  return    : 環境変数(存在しない場合はundef)
sub get_env
{
	my ($env_key) = @_;
	
	$env_key = uc($env_key);
	if(!exists($ENV{$env_key}))
	{
		return undef;
	}

	return $ENV{$env_key};
}

# 環境変数をまとめて取得
#  return    : パラメータのハッシュリファレンス
sub get_envs
{
	my %ret = %ENV;
	
	return \%ret;
}

1;
