#
# MashLib::LogDaily.pm
#
#        author : Masashi Haraki
#

package LogDaily;

use strict;
use warnings;
use utf8;

use Log;

use constant DEFAULT_FORMAT => '{DATE}T{TIME} [{LEVEL}] {FILE}:{LINE} {MESSAGE}';

# コンストラクタ
#  param[in] : 出力先(undef の場合はカレントディレクトリ)
#  param[in] : 出力フォーマット(undef の場合はデフォルト)
#  return    : インスタンス
sub new
{
	my ($myclass, $dir, $format) = @_;
	
	if((!defined($dir)) || ($dir eq ''))
	{
		$dir = './';
	}
	
	if((!defined($format)) || ($format eq ''))
	{
		$format = DEFAULT_FORMAT;
	}
	
	# メンバ
	my $member =
	{
		'log_obj'  => undef,
		'year'     => -1,
		'month'    => -1,
		'day'      => -1,
		'dir'      => $dir,
		'format'   => $format	,
		'file_out' => { 'trace' => 1, 'debug' => 1, 'info' => 1, 'warn' => 1, 'error' => 1, 'fatal' => 1, },
	};
	
	return bless $member, $myclass;
}

# 出力フォーマット設定
#  param[in] : 出力フォーマット
#  return    : 結果
sub set_format
{
	my ($this, $format) = @_;
	
	if((!defined($format)) || ($format eq ''))
	{
		$format = DEFAULT_FORMAT;
	}
	
	$this->{'format'} = $format;
	
	if(defined($this->{'log_obj'}))
	{
		$this->{'log_obj'}->set_format($format);
	}
	
	return 0;
}

# 指定レベルのファイル出力を有効化
#  param[in] : 有効化するレベル('trace', 'debug', 'info', 'warn', 'error' or 'fatal') 複数指定可
#  return    : 結果
sub enable_file_out
{
	my ($this, @levels) = @_;
	my $ret = 0;
	
	foreach my $level (@levels)
	{
		if(!exists($this->{'file_out'}{$level}))
		{
			return -1;
		}
		
		$this->{'file_out'}{$level} = 1;
	}
	
	if(defined($this->{'log_obj'}))
	{
		$this->{'log_obj'}->enable_file_out(@levels);
	}
	
	return 0;
}

# 指定レベルのファイル出力を無効化
#  param[in] : 無効化するレベル('trace', 'debug', 'info', 'warn', 'error' or 'fatal') 複数指定可
#  return    : 結果
sub disable_file_out
{
	my ($this, @levels) = @_;
	my $ret = 0;
	
	foreach my $level (@levels)
	{
		if(!exists($this->{'file_out'}{$level}))
		{
			return -1;
		}
		
		$this->{'file_out'}{$level} = 0;
	}
	
	if(defined($this->{'log_obj'}))
	{
		$this->{'log_obj'}->disable_file_out(@levels);
	}
	
	return 0;
}

# trace 出力
#  param[in] : 文字列
sub trace
{
	my ($this, $message) = @_;
	my ($pkg, $file, $line) = caller();
	
	$this->_print("trace", $pkg, $file, $line, $message);
}

# debug 出力
#  param[in] : 文字列
sub debug
{
	my ($this, $message) = @_;
	my ($pkg, $file, $line) = caller();
	
	$this->_print("debug", $pkg, $file, $line, $message);
}

# info 出力
#  param[in] : 文字列
sub info
{
	my ($this, $message) = @_;
	my ($pkg, $file, $line) = caller();
	
	$this->_print("info", $pkg, $file, $line, $message);
}

# warn 出力
#  param[in] : 文字列
sub warn
{
	my ($this, $message) = @_;
	my ($pkg, $file, $line) = caller();
	
	$this->_print("warn", $pkg, $file, $line, $message);
}

# error 出力
#  param[in] : 文字列
sub error
{
	my ($this, $message) = @_;
	my ($pkg, $file, $line) = caller();
	
	$this->_print("error", $pkg, $file, $line, $message);
}

# fatal 出力
#  param[in] : 文字列
sub fatal
{
	my ($this, $message) = @_;
	my ($pkg, $file, $line) = caller();
	
	$this->_print("fatal", $pkg, $file, $line, $message);
}

# 出力
#  param[in] : 文字列
sub _print
{
	my ($this, $level, $pkg, $file, $line, $message) = @_;
	
	if($this->_check_log_obj() != 0)
	{
		return;
	}
	
	$this->{'log_obj'}->_print($level, $pkg, $file, $line, $message);
}

sub _check_log_obj
{
	my ($this, $level, $pkg, $file, $line, $message) = @_;
	
	my @time = localtime();
	if($this->{'day'} != $time[3])
	{
		$this->{'year'}  = $time[5] + 1900;
		$this->{'month'} = $time[4] + 1;
		$this->{'day'}   = $time[3];
		
		my $filepath = sprintf("%04d/%02d/%02d.log", $this->{'year'}, $this->{'month'}, $this->{'day'});
		
		$this->{'log_obj'} = new Log($this->{'dir'} . $filepath);
		
		print "filepath = " . $this->{'dir'} . $filepath . "\n";
		
		if(defined($this->{'log_obj'}))
		{
			foreach my $level (keys(%{$this->{'file_out'}}))
			{
				if($this->{'file_out'}{$level})
				{
					$this->{'log_obj'}->enable_file_out($level);
				}
				else
				{
					$this->{'log_obj'}->disable_file_out($level);
				}
			}
		}
	}
	
	if(!defined($this->{'log_obj'}))
	{
		return -1;
	}
	
	return 0;
}

1;
