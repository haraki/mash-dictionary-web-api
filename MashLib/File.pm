#
# MashLib::File.pm
#
#        author : Masashi Haraki
#

package File;

use strict;
use warnings;
use utf8;

use File::Path;
use File::Basename qw/basename dirname/;
use Fcntl ':mode';

use Exporter;
our @ISA = qw/Exporter/;
our @EXPORT = qw/read write push_back remove is_exist is_file get_size get_perm make_dir remove_dir is_dir base_name dir_name get_list/;

use constant DEFAULT_FILE_PERM => 0644;
use constant DEFAULT_DIR_PERM => 0755;

# コンストラクタ
#  param[in] : ファイルパス
#  return    : インスタンス
sub new
{
	my ($myclass, $path) = @_;
	
	# メンバ
	my $member =
	{
		'path'   => $path,				# ファイルパス
		'handle' => undef,				# ファイルハンドル
	};
	
	return bless $member, $myclass;
}

# ファイルの一括読み込み
#  param[in]  : ファイルパス or Fileクラスのインスタンス
#  param[out] : 読み込んだデータを格納する配列リファレンス
#  return     : 結果
sub read
{
	if(_is_instance(@_))
	{
		my ($this, $ref_list) = @_;
		
		return _read($this->{'path'}, $ref_list);
	}
	else
	{
		my ($file_path, $ref_list) = @_;
		
		return _read($file_path, $ref_list);
	}
}

# ファイルの一括書き込み(上書き)
#  param[in] : ファイルパス or Fileクラスのインスタンス
#  param[in] : 書き込むデータ
#  return    : 結果
sub write
{
	if(_is_instance(@_))
	{
		my ($this, $data) = @_;
		
		return _write($this->{'path'}, $data);
	}
	else
	{
		my ($file_path, $data) = @_;
		
		return _write($file_path, $data);
	}
}

# ファイルの最後尾に追記
#  param[in] : ファイルパス or Fileクラスのインスタンス
#  param[in] : 追記するデータの配列リファレンス
#  return    : 結果
sub push_back
{
	if(_is_instance(@_))
	{
		my ($this, $data) = @_;
		
		return _push_back($this->{'path'}, $data);
	}
	else
	{
		my ($file_path, $data) = @_;
		
		return _push_back($file_path, $data);
	}
}

# ファイルの消去
#  param[in] : ファイルパス or Fileクラスのインスタンス
#  return    : 結果
sub remove
{
	if(_is_instance(@_))
	{
		my ($this) = @_;
		
		return _remove($this->{'path'});
	}
	else
	{
		my ($file_path) = @_;
		
		return _remove($file_path);
	}
}

# ファイルの存在確認
#  param[in] : ファイルパス or Fileクラスのインスタンス
#  retval 0  : ファイルは存在しない
#  retval 1  : ファイルは存在する
sub is_exist
{
	if(_is_instance(@_))
	{
		my ($this) = @_;
		
		return _is_exist($this->{'path'});
	}
	else
	{
		my ($file_path) = @_;
		
		return _is_exist($file_path);
	}
}

# ファイルかどうかの確認
#  param[in] : ファイルパス or Fileクラスのインスタンス
#  retval 0  : ファイルは存在しない、またはファイルではない
#  retval 1  : ファイルである
sub is_file
{
	if(_is_instance(@_))
	{
		my ($this) = @_;
		
		return _is_file($this->{'path'});
	}
	else
	{
		my ($file_path) = @_;
		
		return _is_file($file_path);
	}
}

# ファイルサイズ取得
#  param[in] : ファイルパス or Fileクラスのインスタンス
#  return    : ファイルサイズ(0以下の場合は存在しない、もしくはファイルではない)
sub get_size
{
	if(_is_instance(@_))
	{
		my ($this) = @_;
		
		return _get_size($this->{'path'});
	}
	else
	{
		my ($file_path) = @_;
		
		return _get_size($file_path);
	}
}

# ファイルパーミッション取得
#  param[in] : ファイルパス or Fileクラスのインスタンス
#  return    : ファイルパーミッション(0の場合は存在しない)
sub get_perm
{
	if(_is_instance(@_))
	{
		my ($this) = @_;
		
		return _get_perm($this->{'path'});
	}
	else
	{
		my ($file_path) = @_;
		
		return _get_perm($file_path);
	}
}

# ディレクトリを生成する
#  param[in] : ディレクトリパス
#  param[in] : パーミッション(指定がない場合は0755)
#  retval  0 : 成功
#  retval  1 : 指定されたディレクトリは既に存在している
#  retval -1 : 失敗
sub make_dir
{
	my ($dir_path, $perm) = @_;
	
	if(!defined($perm))
	{
		$perm = DEFAULT_DIR_PERM;
	}
	
	if(is_dir($dir_path))
	{
		return 1;
	}
	
	eval
	{
		mkpath($dir_path, 0, $perm);	# File::Path::mkpath() はエラーが発生した場合は例外になるのでeval
	};
	
	if($@)							# 例外が発生したか確認（例外の内容が $@ に格納される）
	{
		return -1;
	}
	
	if(!(is_dir($dir_path)))
	{
		return -1;
	}
	
	return 0;
}

# ディレクトリを削除する
#  param[in] : ディレクトリパス
#  retval  0 : 成功
#  retval -1 : 失敗
sub remove_dir
{
	my ($dir_path) = @_;
	
	if(!(is_dir($dir_path)))
	{
		return -1;
	}
	
	eval
	{
		rmtree($dir_path);		# File::Path::rmtree() はエラーが発生した場合は例外になるのでeval
	};
	
	if($@)						# 例外が発生したか確認（例外の内容が $@ に格納される）
	{
		return -1;
	}
	
	if(is_dir($dir_path))
	{
		return -1;
	}
	
	return 0;
}

# ディレクトリかどうかの確認
#  param[in] : ディレクトリパス
#  retval 0  : ディレクトリは存在しない、またはディレクトリではない
#  retval 1  : ディレクトリである
sub is_dir
{
	my ($dir_path) = @_;
	
	if(-d($dir_path))
	{
		return 1;
	}
	
	return 0;
}

# ファイルベース名取得
#  param[in] : パス
#  return    : ファイルベース名
sub base_name
{
	my ($path) = @_;

	return File::Basename::basename($path);
}

# ディレクトリ名取得
#  param[in] : パス
#  return    : ディレクトリ名
sub dir_name
{
	my ($path) = @_;

	return File::Basename::dirname($path);
}

# ファイルリスト取得
#  param[in] : パス
#  return    : ファイルリストのリファレンス
sub get_list
{
	my ($path) = @_;
	my @file_list = glob($path);
	
	return \@file_list;
}

# Fileクラスのインスタンスかチェック
#  param[in] : インスタンス
#  return    : 結果
sub _is_instance
{
	my ($this) = @_;
	
	return ((ref($this)) && ($this->isa(__PACKAGE__))) ? 1 : 0;
}

# ファイルの一括読み込み
#  param[in]  : ファイルパス
#  param[out] : 読み込んだデータを格納する配列リファレンス
#  return     : 結果
sub _read
{
	my ($file_path, $ref_list) = @_;
	
	if(!open(FILE, $file_path))
	{
		return -1;
	}
	
	@$ref_list = <FILE>;
	
	close(FILE);
	
	return 0;
}

# ファイルの一括書き込み(上書き)
#  param[in] : ファイルパス
#  param[in] : 書き込むデータ
#  param[in] : パーミッション(指定がない場合は0644)
#  return    : 結果
sub _write
{
	my ($file_path, $data, $perm) = @_;
	my $ret = 0;
	
	if(!defined($perm))
	{
		$perm = DEFAULT_FILE_PERM;
	}
	
	$ret = _output($file_path, '>', $data);
	
	chmod($perm, $file_path);
	
	return $ret;
}

# ファイルの最後尾に追記
#  param[in] : ファイルパス
#  param[in] : 追記するデータの配列リファレンス
#  return    : 結果
sub _push_back
{
	my ($file_path, $data) = @_;
	my $ret = 0;
	
	$ret = _output($file_path, '>>', $data);
	
	return $ret;
}

# ファイルに出力
#  param[in] : ファイルパス
#  param[in] : モード('>' or '>>')
#  param[in] : 書き込むデータ
#  param[in] : パーミッション(指定がない場合は0644)
#  return    : 結果
sub _output
{
	my ($file_path, $mode, $data) = @_;
	
	my @dirs = split(/\//, $file_path);
	my $filename = pop(@dirs);
	
	# ディレクトリの生成
	local $" = '/';
	my $dir_path = "@dirs";
	
	if($dir_path ne '')
	{
		if(make_dir($dir_path) < 0)
		{
			return -2;
		}
	}
	
	# ファイルのオープン
	if(!open(FILE, $mode . $file_path))
	{
		return -1;
	}
	
	# データをリファレンスにする(元々リファレンスであればそのまま)
	my $type = ref($data);
	my $_data;
	if(!$type)
	{
		$_data = \$data;
		$type = ref($_data);
	}
	else
	{
		$_data = $data;
	}
	
	# データをファイルに出力
	if($type eq "ARRAY")
	{
		print FILE @$_data;
	}
	else
	{
		print FILE $$_data;
	}
	
	close(FILE);
	
	return 0;
}

# ファイルの消去
#  param[in] : ファイルパス
#  return    : 結果
sub _remove
{
	my ($file_path) = @_;
	
	my $remove_num = unlink($file_path);
	
	if($remove_num <= 0)
	{
		return -1;
	}
	
	return 0;
}

# ファイルの存在確認
#  param[in] : ファイルパス
#  retval 0  : ファイルは存在しない
#  retval 1  : ファイルは存在する
sub _is_exist
{
	my ($file_path) = @_;
	
	if(-e($file_path))
	{
		return 1;
	}
	
	return 0;
}

# ファイルかどうかの確認
#  param[in] : ファイルパス
#  retval 0  : ファイルは存在しない、またはファイルではない
#  retval 1  : ファイルである
sub _is_file
{
	my ($file_path) = @_;
	
	if(-f($file_path))
	{
		return 1;
	}
	
	return 0;
}

# ファイルサイズ取得
#  param[in] : ファイルパス
#  return    : ファイルサイズ(0以下の場合は存在しない、もしくはファイルではない)
sub _get_size
{
	my ($file_path) = @_;
	
	if(is_dir($file_path))
	{
		return -1;
	}
	
	my $ret = -s($file_path);
	
	if(!defined($ret))
	{
		return -1;
	}
	
	return $ret;
}

# ファイルパーミッション取得
#  param[in] : ファイルパス
#  return    : ファイルパーミッション(0の場合は存在しない)
sub _get_perm
{
	my ($file_path) = @_;
	
	my $ret = (stat($file_path))[2];
	if(!defined($ret))
	{
		$ret = 0;
	}
	
	return S_IMODE($ret);
}

1;
