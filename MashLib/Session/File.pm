#
# MashLib::Session::File.pm
#
#        author : Masashi Haraki
#

package Session::File;
our @ISA = "Session";

use strict;
use warnings;
use utf8;

use Session;
use File;

use constant DEFAULT_DIR => './.session';

# コンストラクタ
#  return    : インスタンス
sub new
{
	my ($myclass, $dir_path, $expire) = @_;

	if(!defined($dir_path))
	{
		$dir_path = DEFAULT_DIR;
	}

	my $this = $myclass->SUPER::new($expire);

	$this->{'dir_path'} = $dir_path;

	return $this;
}

# 初期化
#  param[in]  : セッションID(undef の場合は新規生成)
#  param[out] : セッションID格納先リファレンス
#  return     : 結果
sub initialize
{
	my ($this, $id, $ref_newid) = @_;

	if(!defined($this->{'dir_path'}))
	{
		return -1;
	}
	
	my $ret = make_dir($this->{'dir_path'});
	if($ret < 0)
	{
		return $ret;
	}
	
	$ret = $this->_initialize("driver:File", $id, {Directory => $this->{'dir_path'}}, $ref_newid);
	
	return $ret;
}

# セッション情報の保存先ディレクトリパスを設定
#  param[in] : ディレクトリパス
#  return    : 結果
sub set_dir_path
{
	my ($this, $dir_path) = @_;
	
	$this->{'dir_path'} = $dir_path;

	return 0;
}

# セッション情報の保存先ディレクトリパスを取得
#  return    : ディレクトリパス(存在しない場合はundef)
sub get_dir_path
{
	my ($this) = @_;
	
	return $this->{'dir_path'};
}

1;
