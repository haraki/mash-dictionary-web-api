#
# MashLib::Param.pm
#
#        author : Masashi Haraki
#

package Param;

use strict;
use warnings;
use utf8;

use CGI;
use Util;

# コンストラクタ
#  return    : インスタンス
sub new
{
	my ($myclass, $cgi_query) = @_;
	
	if(!defined($cgi_query))
	{
		$cgi_query = new CGI();
	}

	# メンバ
	my $member =
	{
		'cgi_query' => $cgi_query,
		'params'    => undef,
		'is_https'  => 0,
		'self_url'  => '',
		'path_info' => (),
	};
	
	my $this = bless $member, $myclass;

	_initialize($this);

	return $this;
}

# 初期化
sub _initialize
{
	my ($this) = @_;

	my @names = $this->{'cgi_query'}->param();
	foreach my $name (@names)
	{
		$name = decode_url($name);
		$this->{'params'}{$name} = decode_url($this->{'cgi_query'}->param($name));
	}

	my $path_info = $this->{'cgi_query'}->path_info();
	if((defined($path_info)) && ($path_info ne ''))
	{
		$path_info =~ s/^\///;			# 先頭の / を削除

		@{$this->{'path_info'}} = split(/\//, $path_info);
	}

	my $https = $this->{'cgi_query'}->https();
	if(defined($https))
	{
		$this->{'is_https'} = 1;
	}
	else
	{
		$this->{'is_https'} = 0;
	}

	$this->{'self_url'} = $this->{'cgi_query'}->self_url();
}

# パラメータの名前一覧の取得
#  return    : 名前一覧の配列のリファレンス(存在しない場合はundef)
sub get_param_names
{
	my ($this) = @_;
	
	if((!exists($this->{'params'})) || (!defined($this->{'params'})))
	{
		return undef;
	}
	
	my @names = keys(%{$this->{'params'}});
	
	return \@names;
}

# パラメータの取得
#  param[in] : パラメータ名
#  return    : パラメータ(存在しない場合はundef)
sub get_param
{
	my ($this, $name) = @_;
	
	if((!defined($name)) || ($name eq ''))
	{
		return undef;
	}
	
	if((!exists($this->{'params'}{$name})) || (!defined($this->{'params'}{$name})))
	{
		return undef;
	}
	
	return $this->{'params'}{$name};
}

# パラメータをまとめて取得
#  return    : パラメータのハッシュリファレンス(存在しない場合はundef)
sub get_params
{
	my ($this) = @_;
	
	if((!exists($this->{'params'})) || (!defined($this->{'params'})))
	{
		return undef;
	}
	
	my %ret = %{$this->{'params'}};
	
	return \%ret;
}

# HTTPS の判定
#  return    : HTTPS であれば 1、そうでなければ 0
sub is_https
{
	my ($this) = @_;

	return $this->{'is_https'};
}

# URL の取得
#  return    : URL(URLの構成に必要なパラメータが存在しない場合はundef)
sub get_url
{
	my ($this) = @_;

	return $this->{'self_url'};
}

# PATH_INFO 配列の取得
#  return    : PATH_INFO 配列のリファレンス(存在しない場合はundef)
sub get_path_info
{
	my ($this) = @_;
	my @ret = ();
	
	if(defined($this->{'path_info'}))
	{
		@ret = @{$this->{'path_info'}}; 
	}

	return \@ret;
}

1;
