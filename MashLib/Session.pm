#
# MashLib::Session.pm
#
#        author : Masashi Haraki
#

package Session;

use strict;
use warnings;
use utf8;

use CGI::Session qw/-ip-match/;
use Util;

use constant DEFAULT_EXPIRE => '+5m';

# コンストラクタ
#  return    : インスタンス
sub new
{
	my ($myclass, $expire) = @_;
	
	if((!defined($expire)) || ($expire eq '') || ($expire <= 0))
	{
		$expire = DEFAULT_EXPIRE;
	}

	# メンバ
	my $member =
	{
		'session' => undef,
		'expire'  => $expire,
	};
	
	my $this = bless $member, $myclass;

	return $this;
}

# 初期化及びセッションの生成
#  param[in]  : セッションID(undef の場合は新規生成)
#  param[out] : セッションID格納先リファレンス
#  return     : 結果
sub initialize
{
	return -1;
}

# セッションを閉じる
#  return    : 結果
sub close
{
	my ($this) = @_;
	
	if(defined($this->{'session'}))
	{
		undef($this->{'session'});
	}
	
	return 0;
}

# セッション情報を保存
#  return    : 結果
sub flush
{
	my ($this) = @_;
	
	if(!defined($this->{'session'}))
	{
		return -1;
	}
	
	$this->{'session'}->flush();
	
	return 0;
}

# セッション情報にデータを設定
#  param[in] : キー
#  param[in] : 値(配列・ハッシュの場合はリファレンス)
#  param[in] : 有効期限(undef の場合はデフォルト)
#  return    : 結果
sub set_param
{
	my ($this, $key, $param, $expire) = @_;
	
	if(!defined($this->{'session'}))
	{
		return -1;
	}
	
	$this->{'session'}->param($key, $param);
	
	if(defined($expire))
	{
		$this->{'session'}->expire($key, $expire);
	}
	
	return 0;
}

# セッション情報からデータを取得
#  param[in]  : キー
#  return     : 値(配列・ハッシュの場合はリファレンス、エラーの場合はundef)
sub get_param
{
	my ($this, $key) = @_;
	
	if(!defined($this->{'session'}))
	{
		return undef;
	}
	
	return $this->{'session'}->param($key);
}

# セッション情報からデータをクリア
#  param[in] : キー
#  return    : 結果
sub clear_param
{
	my ($this, $key) = @_;
	
	if(!defined($this->{'session'}))
	{
		return -1;
	}
	
	$this->{'session'}->clear($key);
	
	return 0;
}

# セッション情報を削除
#  return    : 結果
sub delete
{
	my ($this) = @_;
	
	if(defined($this->{'session'}))
	{
		$this->{'session'}->delete();
		
		$this->{'session'}->clear();
	}
	
	return 0;
}

# 初期化(セッション生成)
#  param[in]  : ドライバ
#  param[in]  : セッションID(undef の場合は新規生成)
#  param[in]  : ドライバの引数
#  param[out] : セッションID格納先リファレンス
#  return     : 結果
sub _initialize
{
	my ($this, $driver, $id, $arg, $ref_newid) = @_;
	
	my $session = new CGI::Session($driver, $id, $arg);
	if(!defined($session))
	{
		return -1;
	}
	
	$this->{'session'} = $session;
	$$ref_newid        = $session->id();
	
	if(defined($this->{'expire'}))
	{
		$session->expire($this->{'expire'});
	}
	
	return 0;
}

1;
