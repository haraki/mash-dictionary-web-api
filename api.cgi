#!/usr/bin/perl

#
# MASH Json Dictionary Web API
#
#        author : Masashi Haraki
#

use strict;
use warnings;
use utf8;

use open IO => ":utf8";

BEGIN { unshift(@INC, './MashLib'); unshift(@INC, './MD'); }

use CGI;
use CGI::Carp qw(fatalsToBrowser);
use Api;

Api::api();

exit;
